#/bin/bash

# File: avc-docker-ssh_proxy/entrypoint.sh

set -e

#-------------------------------------------------------------------------------
#   1. VALIDATIONS
#-------------------------------------------------------------------------------

CONFIG_FILE=/etc/ssh_proxy/ssh_proxy.yml

if [ ! -f "${CONFIG_FILE}" ]; then
	echo "No configuration file can be found at: ${CONFIG_FILE}" >&2
	echo "Exiting." >&2
	exit 1
fi

#-------------------------------------------------------------------------------
#   2. MUNIN-NODE
#-------------------------------------------------------------------------------

MUNIN_NODE_NAME="${MUNIN_NODE_NAME:=ssh_proxy}"
sed -i "s/^\s*#\?\s*host_name .*/host_name ${MUNIN_NODE_NAME}/g" /etc/munin/munin-node.conf

if [ -n "${MUNIN_NODE_ALLOW}" ]; then
	echo "allow ${MUNIN_NODE_ALLOW}" >> /etc/munin/munin-node.conf
fi

# start local munin-node
echo "Starting munin-node: ${MUNIN_NODE_NAME}..."
/etc/init.d/munin-node start

#-------------------------------------------------------------------------------
#   3. SSH TUNNELS
#-------------------------------------------------------------------------------

cat "${CONFIG_FILE}" | yq -r keys[] | while read tunnel; do

	echo
	echo "Configuring tunnel: ${tunnel}..."

	remote_user=`cat "${CONFIG_FILE}" | yq -r .[\"${tunnel}\"].remote_user`
	remote_host=`cat "${CONFIG_FILE}" | yq -r .[\"${tunnel}\"].remote_host`
	identity_file=`cat "${CONFIG_FILE}" | yq -r .[\"${tunnel}\"].identity_file`

	echo "  remote_user: ${remote_user}"
	echo "  remote_host: ${remote_host}"
	echo "  identity_file: ${identity_file}"
	
	# replace "/" by "\/"
	identity_file=`echo "${identity_file}" | sed "s/\\//\\\\\\\\\//g"`

	cat /root/.ssh/config0 \
		| sed "s/SSH_TUNNEL_USER/${remote_user}/" \
		| sed "s/SSH_TUNNEL_HOST/${remote_host}/" \
		| sed "s/SSH_TUNNEL_KEY/${identity_file}/" \
		| sed "s/SSH_TUNNEL/${tunnel}/" \
	>> /root/.ssh/config

	cat "${CONFIG_FILE}" | yq -r .[\"${tunnel}\"].tunnels[] | while read path; do

		echo "  - ${path}"
	
		target_host=localhost
		target_port=

		if echo "${path}" | grep -q ':'; then
			target_port=`echo "${path}" | sed s/[^:]*://`
			port=`echo "${path}" | sed s/:.*//`
			if echo "${target_port}" | grep -q ':'; then
				target_host=`echo "${target_port}" | sed s/:.*//`
				target_port=`echo "${target_port}" | sed s/.*://`
			fi
		else
			port="${path}"
		fi
		
		if echo "${port}" | grep -q "-"; then
		
			from_port=`echo "${port}" | awk -F - '{ print $1 }'`
			to_port=`echo "${port}" | awk -F - '{ print $2 }'`
			
			target_port0="${target_port}"
			
			for port in $(eval echo "{$from_port..$to_port}"); do

				if [ -z "${target_port0}" -o "${target_port0}" = - ]; then
					target_port="${port}"
				else
					target_port="${target_port0}"
				fi
			
				command="0.0.0.0:${port}:${target_host}:${target_port}"

				echo "    Creating SSH tunnel: -L ${command} ${tunnel}"

				autossh -M 0 -N -f -L "${command}" ${tunnel}
				
			done
			
		else
			
			if [ -z "${target_port}" ]; then
				target_port="${port}"
			fi
			
			command="0.0.0.0:${port}:${target_host}:${target_port}"

			echo "    Creating SSH tunnel: -L ${command} ${tunnel}"
			
			autossh -M 0 -N -f -L "${command}" ${tunnel}
		
		fi

	done
	
done

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

echo
echo "Container is up and running."

# keep the container alive
tail -f /dev/null
