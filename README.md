# avc-docker-ssh_proxy

Docker image: avcompris/ssh_proxy

Usage:

	$ docker run -d --name ssh_proxy \
	    -e MUNIN_NODE_NAME=sample-ssh_proxy.avcompris.com \
	    -e MUNIN_NODE_ALLOW=^172\\.17\\.0\\.4$ \
	    -v /etc/ssl/private/jenkins:/etc/ssl/private/jenkins:ro \
	    -v /etc/ssh_proxy:/etc/ssh_proxy:ro \
	    avcompris/ssh_proxy

With the following `/etc/ssh_proxy/ssh_proxy.yml` file:

	jenkins:
		remote_user: infra
		remote_host: proxy.avcompris.com
		identity_file: /etc/ssl/private/jenkins/jenkins.key
		tunnels:
		  - 23751                     # will create a 23751:localhost:23751 tunnel
		  - 23754:2375                # will create a 23754:localhost:2375 tunnel
		  - 23755:172.17.0.3:2375     # will create a 23755:172.17.0.3:2375 tunnel
		  - 23756:172.17.0.3:-        # will create a 23756:172.17.0.3:23756 tunnel
		  - 22101-22110               # will create ten 2210x:2210x tunnels
		  - 22201-22210:22            # will create ten 2220x:localhost:22 tunnels
		  - 22301-22310:172.17.0.4:22 # will create ten 2230x:172.17.0.4:22 tunnels
		  - 22401-22410:172.17.0.4:-  # will create ten 2240x:172.17.0.4:2240x tunnels

No exposed port.
