# File: avc-docker-ssh_proxy/Dockerfile
#
# Use to build the image: avcompris/ssh_proxy

FROM debian:9.11
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
#   1. DEBIAN PACKAGES
#-------------------------------------------------------------------------------

RUN apt-get update

RUN apt-get install -y \
	ntp \
	munin-node \
	autossh \
	ssh-client
	
#RUN apt-get install -y \
#	software-properties-common \
#	gnupg2
#RUN add-apt-repository -y ppa:rmescandon/yq \
#	&& apt-get update \
#	&& apt-get install -y yq

#RUN apt-get install -y \
#	snapd
#RUN snap install yq

RUN apt-get install -y \
	jq \
	python-pip
RUN pip install yq==2.10.0

#-------------------------------------------------------------------------------
#   2. SSH CONFIG
#-------------------------------------------------------------------------------

RUN mkdir /root/.ssh

RUN chmod 700 /root/.ssh

COPY ssh_config0 /root/.ssh/config0

RUN chmod 600 /root/.ssh/config0

#-------------------------------------------------------------------------------
#   7. SCRIPTS
#-------------------------------------------------------------------------------

COPY entrypoint.sh .

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

CMD [ "/bin/bash", "entrypoint.sh" ]

